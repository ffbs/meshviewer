module.exports = function () {
  return {
    // Variables are NODE_ID and NODE_NAME (only a-z0-9\- other chars are replaced with _)
    'nodeInfos': [
      {
        'name': 'Clientstatistik',
        'href': '/grafana/d/zglP5XSWk/node?var-node={NODE_ID}',
        'image': '/grafana/render/d-solo/zglP5XSWk/node?panelId=1&var-node={NODE_ID}&from=now-1d&width=650&height=350&theme=light',
        'title': 'Clientstatistik für {NODE_ID} - weiteren Statistiken'
      },
      {
        'name': 'Trafficstatistik',
        'href': '/grafana/d/zglP5XSWk/node?var-node={NODE_ID}',
        'image': '/grafana/render/d-solo/zglP5XSWk/node?panelId=2&var-node={NODE_ID}&from=now-1d&width=650&height=350&theme=light',
        'title': 'Trafficstatistik für {NODE_ID} - weiteren Statistiken'
      },
      {
        'name': 'Systemlast',
        'href': '/grafana/d/zglP5XSWk/node?var-node={NODE_ID}',
        'image': '/grafana/render/d-solo/zglP5XSWk/node?panelId=4&var-node={NODE_ID}&from=now-1d&width=650&height=350&theme=light',
        'title': 'Systemlast für {NODE_ID} - weiteren Statistiken'
      },
      {
        'name': 'Airtime',
        'href': '/grafana/d/zglP5XSWk/node?var-node={NODE_ID}',
        'image': '/grafana/render/d-solo/zglP5XSWk/node?panelId=5&var-node={NODE_ID}&from=now-1d&width=650&height=350&theme=light',
        'title': 'Airtime für {NODE_ID} - weiteren Statistiken'
      },
      {
        'name': 'Latenz zu respondd',
        'href': '/grafana/d/hb7RV-EZk/parker?var-node={NODE_ID}&fullscreen&panelId=3',
        'image': '/grafana/render/d-solo/hb7RV-EZk/parker?panelId=3&var-node={NODE_ID}&from=now-1d&width=650&height=350&theme=light',
        'title': 'Latenz zu respondd für {NODE_ID}'
      },
      {
        'name': 'Wireguard-Traffic',
        'href': '/grafana/d/hb7RV-EZk/parker?var-node={NODE_ID}&fullscreen&panelId=2',
        'image': '/grafana/render/d-solo/hb7RV-EZk/parker?panelId=2&var-node={NODE_ID}&from=now-1d&width=650&height=350&theme=light',
        'title': 'Wireguard-Traffic für {NODE_ID}'
      }
    ],
    'linkInfos': [
      {
        'name': 'Statistik für alle Links zwischen diese Knoten',
        'image': '/grafana/render/d-solo/zglP5XSWk/node?panelId=7&var-node={SOURCE_ID}&var-linktonode={TARGET_ID}&from=now-1d&&width=650&height=350&theme=light',
        'title': 'Linkstatistik des letzten Tages'
      }
    ],
    'globalInfos': [
      {
        'name': 'Globale Statistik',
        'href': '/grafana/d/LqfM5XSWz/global',
        'image': '/grafana/render/d-solo/LqfM5XSWz/global?panelId=2&from=now-7d&&width=650&height=350&theme=light',
        'title': 'Globale Statistik - weiteren Statistiken'
      }
    ],
    // Array of data provider are supported
    'dataPath': ['/data/'],
    'siteName': 'Freifunk Braunschweig',
    'mapLayers': [
      {
        'name': 'OpenStreetMap',
        'url': '/osm/{z}/{x}/{y}.png',
        'config': {
          'maxZoom': 19,
          'attribution': '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap contributors</a>'
        }
      }
    ],
    // Set a visible frame
    'fixedCenter': [
      // Northwest
      [ 52.4789, 9.95773 ],
      // Southeast
      [ 52.03982, 11.15318 ]
    ],
    'domainNames': [
      {
        'domain': 'ffbs',
        'name': 'Braunschweig'
      }
    ],
    'linkList': [
      {
        'title': 'Impressum',
        'href': 'https://freifunk-bs.de/impressum.html'
      }
    ]
  };
};
